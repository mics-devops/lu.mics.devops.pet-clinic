FROM openjdk:8-jdk-alpine
COPY spring-petclinic-2.2.0.BUILD-SNAPSHOT.jar spring-petclinic-2.2.0.BUILD-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java -jar /spring-petclinic-2.2.0.BUILD-SNAPSHOT.jar"]
